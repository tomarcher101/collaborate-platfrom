import "./App.css";
import { Routes, Route } from "react-router-dom";
import { MissionFeed, CreateMission, NavBar, Mission, Profile } from "./components";

function App() {
  return (
    <div className="App">
      <NavBar />
      <body className="body">
        <div className="body-container">
          <Routes>
            <Route path="/" element={<MissionFeed />} />
            <Route path="/mission" element={<Mission />} />
            <Route path="/createMission" element={<CreateMission />} />
            <Route path="/profile" element={<Profile />} />
          </Routes>      
        </div>
      </body>
    </div>
  );
}

export default App;
