import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { makeStyles } from "@material-ui/styles";
import { ButtonPrimary } from "../index";
import tick from "../../assets/img/tick.svg";
import styles from "./requirements-table.module.scss";

const RiskRole = ({risk}) => {
  return (
    <TableRow
      key={risk}
      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
    >
      <TableCell
        component="th"
        scope="row"
        sx={{ fontSize: 18, fontWeight: 600, maxWidth: 200 }}
      >
        { risk }
      </TableCell>
    </TableRow>
  );
};

const RisksTable = ({ risks }) => {
  debugger
  
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650, border: 2 }} aria-label="simple table">
        <TableBody>
          { risks.map(risk => <RiskRole risk={risk}/>)}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default RisksTable;
