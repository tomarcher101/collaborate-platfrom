import React from 'react'
import styles from "./tag-group.module.scss"
import { Tag } from "../../components"

const TagGroup = ({header, tags}) => {
	return (
		<div className={styles["container"]}>
			<div className={styles.header}>{header}</div>
			<div className={styles.tagContainer}>
				{ tags.map(tag => <Tag tag={tag}/>)}
			</div>
		</div>
	)
}

export default TagGroup