import React from "react";
import { Link } from 'react-router-dom';
import thumbnail from "../../assets/img/mission-thumbnail.png"

import styles from "./mission-feed-item.module.scss"

const MissionFeedItem = ({owner, title, description}) => {

  return (
    <Link to="/mission" className={`${styles["container"]} flex flex-col rounded-sm max-w-screen-md p-4 gap-4`}>
      <div className="top-bar flex items-center">
				<img src={thumbnail} alt="" className={styles["thumbnail"]}/>
        <div className="flex flex-col">
          <div style={{color: "#6F1DF4", fontWeight: 700, fontSize: "14px"}}>Name</div>
          <div style={{color: "rgba(60, 60, 67, 0.5)", fontSize: "14px", fontWeight: 400}}>@username</div>
        </div>
      </div>
      <div className="flex items-start justify-start">
				{description}
			</div>
      <div className="flex items-start justify-between">
				<div style={{color: "rgba(60, 60, 67, 0.5)", fontWeight: 400, fontSize: "14px"}}>View Original</div>	
			</div>
    </Link>
  );
};

export default MissionFeedItem;
