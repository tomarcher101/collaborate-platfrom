import React from "react";
import missionHero from "../../assets/img/create-mission-hero.png";
import { TextField, Button } from "@mui/material";
import { DropdownCheckbox, ButtonPrimary } from "..";
import styles from "./create-mission.module.scss";

const skills = ["Driver", "Web Developer", "Dancer"];
const risks = ["Death", "Injury", "Fire"];
const helpers = [
  "Dan Atherton",
  "Luke Ruocco",
  "Marcus Thornley",
  "Tom Archer",
];

const CreateMission = () => {
  return (
    <div className={styles["container"]}>
      <img src={missionHero} className={styles["hero-image"]} />
      <div className={styles["input-container"]}>
        <TextField id="outlined-helperText" label="Mission Title" fullWidth />
      </div>
      <div className={styles["input-container"]}>
        <TextField
          className={styles["multiline"]}
          id="outlined-helperText"
          label="Mission Description"
          fullWidth
          multiline
          inputProps={{ height: 300 }}
        />
      </div>
      <div className={styles["input-container"]}>
        <DropdownCheckbox
          id="skill-dropdown"
          label="Add Skill Requirements"
          choices={skills}
        />
      </div>
      <div className={styles["input-container"]}>
        <DropdownCheckbox
          id="risk-dropdown"
          label="Add Risks"
          choices={risks}
        />
      </div>
      <div className={styles["input-container"]}>
        <DropdownCheckbox
          id="helper-dropdown"
          label="Add Helpers to your mission"
          choices={helpers}
        />
      </div>
      <div className={styles["actions"]}>
        <Button variant="contained" color="error">
          Delete Mission
        </Button>
        <ButtonPrimary variant="contained">Start Mission</ButtonPrimary>
      </div>
    </div>
  );
};

export default CreateMission;
