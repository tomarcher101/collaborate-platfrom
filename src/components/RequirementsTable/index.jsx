import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { makeStyles } from "@material-ui/styles";
import { ButtonPrimary } from "../index";
import tick from "../../assets/img/tick.svg";
import styles from "./requirements-table.module.scss";

const useStyles = makeStyles((theme) => ({
  root: {
    fontSize: "200pt",
  },
  table: {
    fontSize: "200pt",
  },
}));

const ActionCell = ({ number, filledNumber, isFilled, setUserIsIn }) => {
  const [isJoined, setIsJoined] = useState(false);

  return (
    <TableCell align="right" sx={{ width: 170 }}>
      {isJoined ? (
        <ButtonPrimary
          backgroundColor="#5CD084"
          onClick={() => {
            setIsJoined(false);
            setUserIsIn(false);
          }}
        >
          <img className={styles.tick} src={tick} alt="" />
          Joined
        </ButtonPrimary>
      ) : (
        <>
          {isFilled ? (
            <div style={{ fontSize: 18, fontWeight: 600 }}>Role Full</div>
          ) : (
            <ButtonPrimary
              onClick={() => {
                setIsJoined(true);
                setUserIsIn(true);
              }}
            >
							Join
            </ButtonPrimary>
          )}
        </>
      )}
    </TableCell>
  );
};

const RoleRow = ({ name, number, description, filledNumber }) => {
  const [userIsIn, setUserIsIn] = useState(false);
	const [nowFilled, setNowFilled] = useState(filledNumber)
  const [isFilled, setIsFilled] = useState(filledNumber >= number)

	useEffect(() => {
		setNowFilled(filledNumber + (userIsIn ? 1 : 0))
	}, [userIsIn])

	useEffect(() => {
		if (nowFilled >= number) {
			setIsFilled(true)
		} else {
			setIsFilled(false)
		}
	}, [nowFilled])

  return (
    <TableRow
      key={name}
      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
    >
      <TableCell
        component="th"
        scope="row"
        sx={{ fontSize: 18, fontWeight: 600, maxWidth: 200 }}
      >
        {number} x {description}
      </TableCell>
      <TableCell sx={{ fontSize: 18, fontWeight: 600 }}>
        {number - nowFilled} spaces left!
      </TableCell>
      <ActionCell isFilled={isFilled} setUserIsIn={setUserIsIn} />
    </TableRow>
  );
};

const RequirementsTable = ({ requirements }) => {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650, border: 2 }} aria-label="simple table">
        <TableBody>
          {requirements.map((row) => (
            <RoleRow
              number={row.number}
              name={row.name}
              description={row.description}
              filledNumber={row.filledNumber}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default RequirementsTable;
