import React, { useState } from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";

const DropdownCheckbox = ({ choices, label, id }) => {
  const [selectedChoices, setSelectedChoices] = useState([]);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setSelectedChoices(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };

  return (
    <FormControl fullWidth>
      <InputLabel id={id}>{label}</InputLabel>
      <Select
        labelId={id}
        multiple
        value={selectedChoices}
        onChange={handleChange}
        input={<OutlinedInput label={label} />}
        renderValue={(selected) => selected.join(",  ")}
        fullWidth
        // MenuProps={MenuProps}
      >
        {choices.map((choice) => (
          <MenuItem key={choice} value={choice}>
            <Checkbox checked={selectedChoices.indexOf(choice) > -1} />
            <ListItemText primary={choice} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default DropdownCheckbox;
