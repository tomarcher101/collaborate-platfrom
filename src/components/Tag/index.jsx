import React from 'react'
import styles from "./tag.module.scss"

const Tag = ({tag}) => {
	return (
		<div className={styles["container"]}>
			{tag}
		</div>
	)
}

export default Tag