import React from "react";
import logo from "../../assets/img/logo.png";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import { TextField } from "@mui/material";
import styles from "./nav-bar.module.scss";
import { Link } from "react-router-dom";
import { ButtonPrimary } from "../../components";

import profile from "../../assets/img/dan-profile.png"

const NavBar = () => {
  return (
    <div className={styles["container"]}>
      <div className={styles["logo-container"]}>
        <Link to="/">
          <img className={styles.logo} src={logo} alt="" />
        </Link>
      </div>
      <div className={styles["search-container"]}>
        <TextField
          id="outlined-basic"
          label="Search for mission"
          variant="outlined"
          fullWidth
          size="small"
        />
      </div>
      <div className={styles["action-container"]}>
        <ButtonPrimary className={styles["button"]} variant="contained">
          <Link to="/createMission">Create Mission</Link>
        </ButtonPrimary>
        <Link to="/profile">
          <img src={profile} alt="" className={styles["user-icon"]}/>
        </Link>
      </div>
    </div>
  );
};

export default NavBar;
