import React, { useState } from "react";
import { Box, Tabs, Tab } from "@mui/material";
import mission1 from "../../data/mission1.json";
import { MissionFeedItem } from "../../components";

const MissionFeed = () => {
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className="flex flex-col items-center">
      <Box sx={{ borderBottom: 1, borderColor: "divider" }} className="">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          centered
        >
          <Tab label="All Missions" value={0} />
          <Tab label="Recommended Missions" value={1} />
        </Tabs>
      </Box>
      {value == 0 && (
        <div className="flex flex-col gap-y-10 p-3">
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
        </div>
      )}
      {value == 1 && (
        <div className="flex flex-col gap-y-10 p-3">
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
          {mission1.map((mission) => (
            <MissionFeedItem
              owner={mission.owner}
              description={mission.description}
            />
          ))}
        </div>
      )}
    </div>
  );
};

export default MissionFeed;
