import React, { useState } from "react";
import styles from "./profile.module.scss";
import profile from "../../assets/img/dan-profile.png";
import leaderIcon from "../../assets/img/leader-icon.png";
import twitterIcon from "../../assets/img/twitter-icon.png";
import linkedinIcon from "../../assets/img/linkedin-icon.png";
import { TagGroup, MissionFeedItem } from "../../components";
import { Box, Tabs, Tab } from "@mui/material";
import mission1 from "../../data/mission1.json";

const skills = [
  "driver",
  "bilingual",
  "sign language",
  "cooking",
  "heavy lifting",
];
const resources = ["van", "sleeping bag", "spare laptop"];

const Profile = () => {
  const [tabValue, setTabValue] = useState(0);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  return (
    <div className={styles["container"]}>
      <div className={styles["banner"]}>
        <img src={profile} alt="" />
        <div className={styles["details"]}>
          <div className={styles["name"]}>
            Dan Atherton
            <div className={styles["links"]}>
              <img src={twitterIcon} alt="" />
              <img src={linkedinIcon} alt="" />
            </div>
          </div>
          <div className={styles["role"]}>
            <img src={leaderIcon} alt="" />
            <div>Leader</div>
          </div>
        </div>
      </div>
      <div className={styles.bio}>
        <div className={styles.header}>Bio</div>
        <div className={styles.description}>
          Hi - early 40s delivery driver. Speak fluent Donnie and am in good
          physical shape - happy to help on more manual missions. Committed to
          refugee, disability and environmental causes. BSL trained.
        </div>
      </div>
      <div className={styles.skills}>
        <TagGroup header="Skills" tags={skills} />
      </div>
      <div className={styles.resources}>
        <TagGroup header="Resources" tags={resources} />
      </div>
      <div className={styles.missions}>
        <div className={styles.header}>Missions</div>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }} className="">
          <Tabs
            value={tabValue}
            onChange={handleChange}
            aria-label="basic tabs example"
            centered
          >
            <Tab label="Active Missions" value={0} />
            <Tab label="Completed Missions" value={1} />
          </Tabs>
        </Box>
        {tabValue == 0 && (
          <div className="flex flex-col gap-y-10 p-3">
            {mission1.map((mission) => (
              <MissionFeedItem
                owner={mission.owner}
                description={mission.description}
              />
            ))}
          </div>
        )}
        {tabValue == 1 && (
          <div className="flex flex-col gap-y-10 p-3">
            {mission1.map((mission) => (
              <MissionFeedItem
                owner={mission.owner}
                description={mission.description}
              />
            ))}
            {mission1.map((mission) => (
              <MissionFeedItem
                owner={mission.owner}
                description={mission.description}
              />
            ))}
            {mission1.map((mission) => (
              <MissionFeedItem
                owner={mission.owner}
                description={mission.description}
              />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export default Profile;
