import React, { useState } from "react";
import styles from "./mission.module.scss";
import missionHero from "../../assets/img/mission-hero.png";
import missionData from "../../data/mission1.json";
import { ButtonPrimary, RequirementsTable, RisksTable } from "../index.jsx";
import tickJoined from "../../assets/img/tick-joined.png";
import danProfile from "../../assets/img/dan-profile.png";
import Countdown from "react-countdown";

const mission = missionData[0];

const Mission = () => {
  const [hasJoinedMission, setHasJoinedMission] = useState(false);

  return (
    <>
      {!hasJoinedMission ? (
        <div className={styles["container"]}>
          <img src={missionHero} className={styles["hero-image"]} />
          <h1 className={styles.title}>{mission.title}</h1>
          <div className={styles.description}>{mission.description}</div>
          <div className={styles.requirements}>
            <h3 className={styles.header}>Requirements</h3>
            <RequirementsTable requirements={mission.requirements} />
          </div>
          <div className={styles.risks}>
            <h3 className={styles.header}>Risks</h3>
            <RisksTable risks={mission.risks} />
          </div>
          <div className={styles.actions}>
            <ButtonPrimary onClick={() => setHasJoinedMission(true)}>
              Join Mission
            </ButtonPrimary>
          </div>
        </div>
      ) : (
        <div className={styles["container-joined"]}>
          <div className={styles.tickContainer}>
            <img src={tickJoined} alt="" />
            <img src={danProfile} alt="" />
          </div>
          <div className={styles.textSection}>
            <h3>Congratulations Dan!</h3>
            <h3>You're on a mission!</h3>
          </div>
          <div className={styles.textSection}>
            <p>{mission.description}</p>
          </div>
          <div className={styles.textSection}>
            <h3>Mission Countdown</h3>
            <div className={styles.textSection}>
              <Countdown
                date={Date.now() + 1000000000}
                intervalDelay={0}
                renderer={(props) => (
                  <div className={styles.countdown}>
                    {props.days}d : {props.hours}hrs : {props.minutes}minutes
                  </div>
                )}
              />
            </div>
          </div>
          <div className={styles.textSection}>
            <h3>Mission Details</h3>
            <p>Meeting Point: TBC</p>
            <p>Key Contact: Marcus Thornely</p>
            <p>Contact Number: 07585993435</p>
          </div>
          <div className={styles.textSection}>
            <h3>Share this with another helper to support the cause!</h3>
          </div>
          <div className={styles.actions}>
            <ButtonPrimary onClick={() => setHasJoinedMission(true)}>
              Share Mission 🤝
            </ButtonPrimary>
          </div>
        </div>
      )}
    </>
  );
};

export default Mission;
