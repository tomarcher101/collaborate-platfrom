import React from "react";
import { Button } from "@mui/material";

const ButtonPrimary = (props) => {
  return (
    <Button
      style={{
        backgroundColor: props.backgroundColor || "#6F1DF4",
        color: props.color || "#FFFFFF",
      }}
			{...props}
    >
      {props.children}
    </Button>
  );
};

export default ButtonPrimary;
